//console.log("hello world")

// repetition control structures

// while loop 
/*
	-a while loop takes in a condition/expression
	-if the condition evaluated to be true the statements inside the code will be exected
	synatx:
	while (expression/condition){
		statement
	}

*/

// let count = 5;

//while the value of count is not equal to zero, it will run
// while (count !==0 ){
// 	//display value of coundt
// 	console.log("while:" + count);

// 	//decrease the value of 1 after evry iteration 
// 	count--;	
// }


// do while loop 
/*
	works a alot like while loop 
	but it  guratees that it will run at least once 
	
	syntax
	do{
		statement
	}while (expression)

*/

 //let number = Number(prompt("give me a number"))

// do {
// 	//the current value of number is printed out
// 	console.log("do while " + number);
// 	//increse the number by 1
// 	number +=1;
// //providing a number of 10 or greater will run the code block once but the loop will stop there
// }while(number <= 10);

// for loop
/*
	a for loop is more flexible than
	 while and do while loops
	 	3 parts
	 	1. the initial value 
	 	2. the expression (while this is true the loop will run)
	 	3. final expression (increment or decrement )
	 	syntax
	 	for(intitial; condition; increment/decrement){
	 	}
*/

/*
this loop start from 0
this loop will run aaslong the number is equal to zero
this loop 1 after every run
*/
for (let count=0; count<=20; count++){
	console.log(count);
}

let myString = "jake";
console.log(myString.length)

// accessing elements of a string
console.log(myString[2]);

//this loop prints the individual value of variable myString
for(let x=0; x <=myString.length; x++){
	console.log(myString[x]);
}

//this loop print out the letter of the name individually but will print 3 if the letter is a vowel 


// let myName = "johndaniel";
// for (var i = 0; i < myName.length; i++) {
// 	if (
// 		myName[i].toLowerCase()=="a"||
// 		myName[i].toLowerCase()=="e"||
// 		myName[i].toLowerCase()=="i"||
// 		myName[i].toLowerCase()=="o"||
// 		myName[i].toLowerCase()=="u"
// 		){
// 		console.log(3);
// 	} 
// 	else{
// 		console.log(myName[i]);
// 	}
// }


//continue and break
/*
	the continue statement allows the code to go the next iteration of a loop without finishing the execution of a statements in a code block

	the break statement is used to terminate the loop once a match has been found 
*/



// for(let count = 0; count<=20; count++){
// 	//if remainder is equal to 0
// 	if (count % 2 === 0){
		
// 		continue;
// 	}
// 	console.log("continue and break " + count);
// 	if(count>10){
// 		break;
// 	}
// }



let name = "jakelexdter";

for(let i=0; i <name.length;i++){
	console.log(name[i]);
	if (name[i].toLowerCase() === "a"){
		console.log("continue to next iteration");
		continue;
	}
	if (name[i]==="d"){
		break;
	}
}





